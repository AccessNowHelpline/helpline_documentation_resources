---
title: 'Access Now Digital Security Helpline Intermediary Process'
keywords: vetting, process
classification: Public
abstract: The following document defines Access Now Digital Security Helpline's vetting process for new clients
---

# Access Now Digital Security Helpline

# Vetting Process

## Purpose of vetting

The purpose of vetting clients is an exercise in reducing risk for Access Now and for users at risk.

Some of the risks mitigated by vetting include the risk to Access Now of reputation damage resulting 
from working with organizations that themselves do not uphold basic human rights, or are controversial 
for any other reason. There is the risk of being socially engineered by our adversaries into releasing 
information, or allowing our adversaries into getting a foothold onto our platforms, that would then 
allow them to perpetrate effective attacks on our operation. There is also the risk of adversaries 
consuming our resources in fake incidents, thus denying capability to the people and organizations 
that really require our assistance.

Vetting is an exercise of doing adequate due diligence with the clients we assist, to ensure they are truly users at risk.

To make sure this vetting process is properly recorded, the operator will create a “child case” in the ticketing system, 
under the administration queue. All communications needed to complete the  vetting process will be recorded in 
a chronological order in the case history.


## Access Now Digital Security Helpline's Vetting Process 

The process used to vet all new clients consists in the following steps:

1. Initial evaluation
2. Identify/contact potential vettors
3. Evaluation of responses
4. Sign off and recording


### 1. Initial evaluation

A certain amount of groundwork can be done initially via information sources such as Google, Wikipedia, 
the organization's own website, Whois, PGP keyservers, etc., to make a determination of the validity of 
the organization and individual/s in question. None of these sources alone should be considered reliable, 
but together it is possible to get some sense of the legitimacy of the organization/individual.


### 2. Identify/contact potential vettors

Identifying who are potential vettors is the next step. We need to find someone we already know and trust 
that is prepared to vouch for the potential new client.

A good place to start is to look at the organization's website, particularly any pages identifying members 
of the organization's board. Board members are often high-profile people in the NGO space, and are frequently 
known to staff at Access Now, so this presents an excellent way to identify potential vettors.


### 3. Evaluation of responses

What we are trying to establish is that the client is who they claim they are, and that they act rationally 
and with safety and respect for others. It is important that the client has the capacity to respect Access Now's 
reputation if we are to involve our organization in providing them assistance, so largely for us this is what 
we are actually trying to determine.

This is never going to be a hard and fast ruling of "adequacy", as the nature of the trust relationships are 
always going to be somewhat subjective. However as a general heuristic rule we can consider that if someone 
we trust implicitly vouches for a new client we can consider them vetted. If we cannot find someone that we 
trust implicitly, then we would need two acquaintances whose reputations we trust, that both vouch for the 
new client before we would consider the vetting adequate.


### 4. Sign off and recording

Each vetting process needs to be signed off by the technology management (Chief Technologist, Manager of Security 
Education, and Incident Response Manager).

The fact that the client has been through the vetting process and either been declined or successfully vetted 
is recorded in the Access Now Digital Security Helpline's ticketing system.