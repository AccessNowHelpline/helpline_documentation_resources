# Access Now Digital Security Helpline resources and documentation

In this repository, you can find templates for security policies and assessment reports
used by [Access Now Digital Security Helpline](https://www.accessnow.org/help),
as well as documents and scripts we share with the public.

The Helpline's public documentation on how to handle cases can be found at [https://communitydocs.accessnow.org](https://communitydocs.accessnow.org) ([Gitlab repository](https://gitlab.com/AccessNowHelpline/community-documentation)).

The Helpline's end-user guides can be found at [https://guides.accessnow.org](https://guides.accessnow.org) ([Gitlab repository](https://gitlab.com/AccessNowHelpline/guides)).
