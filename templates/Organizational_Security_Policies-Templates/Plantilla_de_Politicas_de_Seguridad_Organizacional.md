Plantilla de Políticas de Seguridad Organizacional
=================

Prácticas organizacionales generales
-------------------

1. [Políticas de Acceso](#7we3ntqs92fn)
    - [a. Acceso al área de trabajo](#llduhfrf8ye0)
        - [i. La oficina y el escritorio](#5cy5di7ujjex)
        - [ii. Oficina en casa o espacio de trabajo personal](#corws3uqqxe7)
    - [b. Acceso a documentos físicos](#nu5wl7mbnh5)
        - [i. Documentos impresos](#u9fp01cjk71z)
        - [ii. Destrucción de documentos](#ohynrrsowkja)
    - [c. Dispositivos](#cneaewojyb0j)
        - [i. Número de dispositivos](#4dhi7dsh04tw)
        - [ii. Acceso a dispositivos](#wu4v2aytkvmo)
    - [e. Acceso a cuentas](#v5nwlrejiitd)
        - [i. Autenticación de dos pasos](#wtfwcz9ih5gb)
        - [ii. Administración de contraseñas](#6r1cvdme4q45)
        - [iii. Google Docs](#ups1ji8w8qx)
    - [b. Revocando acceso a los activos al salir de la organización](#jsgpqgjiss5a)

2. [Políticas de administración de datos](#qi8barmv5j12)
    - [a. Respaldo de datos](#d1n4qtbvkrzi)
    - [b. Transferencia de datos](#tcdwcnkg8nw6)
    - [c. Viajes](#kebchkxtl2yd)
    - [d. Borrados de datos](#3s9d98tikn3q)
    - [e. Cifrado de dispositivos](#g47kclkb06ga)
    - [f. Cifrado de medios extraibles](#x4056g72bwvp)

3. [Políticas y prácticas de comunicación](#2mjrr3say9pg)
    - [a. Comunicación en línea](#v1i5hop72acs)
    - [b. Comunicación con personas en riesgo](#l3uv2efruza2)
    - [c. Comportamiento en la web](#fld08f9e02f3)
    - [d. Navegación web](#lk3pokycowon)
    - [e. Preocupaciones de seguridad digital](#gqy8gv5w1rzc)

4. [Políticas y procedimientos de viaje](#cwhasyrukd3s)
    - [a. Dispositivos](#66ctgfndt3o)
    - [b. Uso de Internet](#wf4menzgiaqi)

5. [Creando y manteniendo una cultura organizacional comprometida](#6bqo8dqlb1im)

* * *

<a name="7we3ntqs92fn"></a>

### 1. Políticas de Acceso

<a name="llduhfrf8ye0"></a>

#### a. Acceso al área de trabajo

<a name="5cy5di7ujjex"></a>

##### I. La oficina y el escritorio

-   **¿Quien tiene las llaves, tarjetas de acceso o PIN para ingresar a la oficina?**

    -   Llevar control de cuántas copias de llaves o tarjetas de acceso
        se tienen. Estas sólo se deben entregar y compartir los PINs a
        las personas de confianza para permanecer solos en la oficina.

    -   Recuperar las llaves y tarjetas cuando un miembro del staff deja
        la organización. Los PINs deben ser cambiados.

    -   Evitar prestar las llaves o tarjetas a cualquiera que no sea un
        miembro permanente del staff. Evitar compartir los PINs.

-   **Cerraduras en escritorios, puertas y gabinetes**

    -   La última persona en salir de la oficina debe cerrar con llave.

    -   Los documentos y dispositivos importantes deben ser almacenados
        en un escritorio con llave o gabinete.

-   **Cuidado con los impostores y quien te sigue: **La** **“Ingeniería
    social” es la técnica que consiste en manipular a otros a tomar una
    acción, comúnmente aprovechándose de la amabilidad, el apego a la
    rutina o el respeto a la autoridad de la víctima.

    Un ingeniero social puede intentar seguirnos en la entrada al edificio
con el objetivo de ingresar sin autorización, podría por ejemplo
aprovecharse de nuestra amabilidad de sostener la puerta o pretendiendo
que busca la llave o tarjeta de acceso en sus bolsillos o mochila
esperando evocar un sentimiento de lástima.

    Impersonar a otra persona es otra forma en la que alguien puede ingresar
a un edificio. Tal vez vestidos como personal de mantenimiento,
mensajero, o alguien esperando para una entrevista. En organizaciones
grandes es fácil distraer a la víctima con la exusa de ser nuevo en el
trabajo o de un departamento diferente.

    Nunca debemos sentirnos incómodos o extraños de preguntar sobre los
motivos de alguien para estar donde están, cuestionar su trabajo o
solicitar que se identifiquen. Si están en el lugar correcto no les
molestara responder nuestras preguntas.

<a name="corws3uqqxe7"></a>

##### II. Oficina en casa o espacio de trabajo personal

-   **¿Quién tiene acceso a nuestra oficina en casa?**

    Tener control sobre quién tiene acceso a nuestra oficina en casa, de
amigos a familiares, dejarles claro la confidencialidad de nuestro
trabajo. Si un extraño está en nuestra casa, e.g. personal de limpieza o
mantenimiento, debemos evitar que tengan acceso a nuestro lugar de
trabajo. Si el extraño necesita acceso a este espacio, debemos tomar
precauciones antes de su visita (guardar con llave los documentos
confidenciales, guardar laptops y apagar las computadoras de escritorio)

-   **Cerraduras en escritorios, puertas y gabinetes**

    Al final del dia de trabajo, los documentos confidenciales deben ser
guardados bajo llave y de ser posible fuera del alcance de la vista.
Laptops y computadoras de escritorio deben ser apagadas y puestas fuera
de vista.

<a name="nu5wl7mbnh5"></a>

#### b. Acceso a documentos físicos

<a name="u9fp01cjk71z"></a>

##### I. Documentos impresos

Cualquier documento impreso con información sobre {ORG} debe ser
tratado como confidencial y darle el cuidado apropiado. No dejar estos
documentos visibles en espacios públicos y debemos estar conscientes
de que tan privada es la información antes de imprimirla --
especialmente si vamos a realizar copias.

Debemos evitar llevar versiones impresas de documentos confidenciales
a casa y evitar utilizar material impreso cuando se trabaja desde un
lugar público (cafés, hoteles, aeropuertos). Nunca se debe dejar
desatendido ningún material impreso.

Cuando estamos en la oficina, debemos evitar dejar documentos impresos
visibles sobre nuestro escritorio, y tratemos de resguardar cualquier
documento bajo llave en un escritorio o gabinete antes de dejar la
oficina. <span id="anchor-6"></span>Cuando estamos en la oficina,
debemos evitar dejar documentos impresos visibles sobre nuestro
escritorio, y tratemos de resguardar cualquier documento bajo llave en
un escritorio o gabinete antes de dejar la oficina.

<a name="ohynrrsowkja"></a>

##### II. Destrucción de documentos

Cuando un documento es impreso, tan pronto como deje de ser necesario,
debe ser destruido en una trituradora de papel lo antes posible.

<a name="cneaewojyb0j"></a>

#### c. Dispositivos

<a name="4dhi7dsh04tw"></a>

##### I. Número de dispositivos

Debemos intentar reducir el número de dispositivos que contienen
información sobre el trabajo, incluyendo las credenciales de cuentas
de correo electŕonico. Cada nuevo dispositivo con información
confidencial o acceso a cuentas de trabajo es un nuevo dispositivo que
necesitará protección contra pérdida, robo, y hackeo. De ser posible,
un smartphone y una computadora (de escritorio o portátil), deben ser
los únicos dispositivos utilizados con información de trabajo

{ORG} ha suministrado computadoras de trabajo a los colaboradores con
el objetivo de asegurar que la información confidencial permanezca en
estos dispositivos y no sea transferida a dispositivos personales.

<a name="wu4v2aytkvmo"></a>

##### II. Acceso a dispositivos

Debemos asegurar que solo el dueño del dispositivo tiene acceso al
mismo, si hay información confidencial en la misma computadora que
utilizan terceros no relacionados con {ORG} esta debe ser removida.

Todos los dispositivos que contengan información sobre {ORG} deben
estar protegidos por una contraseña o PIN, incluyendo los smartphones.

Cuando se habilita el cifrado de dispositivos, una contraseña de
acceso es creada por defecto. Ver la sección [*Cifrado de
dispositivos*](#g47kclkb06ga) para más detalles.

<a name="v5nwlrejiitd"></a>

#### e. Acceso a cuentas

<a name="wtfwcz9ih5gb"></a>

##### I. Autenticación de dos pasos

Todos los programas deben tener habilitada la autenticación de dos
pasos cuando sea posible. Esto incluye:

-   Google Apps (gmail, drive, etc)

    - [*Guía de autenticación de dos pasos de Google*](https://www.google.com/landing/2step/help.html)

    -   [*Google 2FA FAQ*](https://docs.google.com/a/accessnow.org/document/d/1whY3W_JLXRytmjYxZdYVKcE9trei4eMXRGcchPoQG3Y/edit?usp=sharing)

-   Dropbox

    -   [*Guía de Dropbox 2FA*](https://www.dropbox.com/es/help/363)

<a name="6r1cvdme4q45"></a>

##### II. Administración de contraseñas

-   **Contraseñas personales**

    La creación responsable de contraseñas y su utilización son explicados en los siguientes enlaces

    -   [*https://ssd.eff.org/es/module/creando-contrase%C3%B1as-seguras*](https://ssd.eff.org/es/module/creando-contrase%C3%B1as-seguras)

    -   [*https://securityinabox.org/es/chapter-3*](https://securityinabox.org/es/chapter-3)

    Las conclusiones principales son:

    -   *Longitud*: más de 15 caracteres

    -   *Robustez*: Debe contener letras mayúsculas y minúsculas, números, y símbolos.

    -   *Únicas*: Nunca reutilizar una contraseña

    ¿Estos requerimientos son intimidantes? Considere la siguiente
historieta como inspiricación: [*https://xkcd.com/936/*](https://xkcd.com/936/)

-   **Administrador de contraseñas**

    Para administrar las contraseñas personales y asegurar que las mismas son largas, robustas y únicas se debe utilizar un administrador de contraseñas

    La herramienta recomendada para este fin es KeePassXC:

[*https://ssd.eff.org/es/module/c%C3%B3mo-usar-keepassxc*](https://ssd.eff.org/es/module/c%C3%B3mo-usar-keepassxc)

[*https://securityinabox.org/es/keepass\_main*](https://securityinabox.org/es/keepass_main)

-   **Contraseñas compartidas**

    Si es necesario compartir contraseñas con un colega, debemos utilizar una estrategia que reduzca el riesgo de que la contraseña sea obtenida por un tercero o comprometida.

    En particular, compartirla en un trozo de papel o post-it puede ser
buena idea siempre y cuando la contraseña sea introducida en el
administrador de contraseñas del receptor y luego el trozo de papel es
destruido.

    No es recomendable utilizar servicios de terceros como **Keyvault, ** ya que estaríamos compartiendo información sensible con un tercero y el contenido no está cifrado de extremo-a-extremo (el cifrado extremo-a-extremo garantiza que solo el emisor y el receptor pueden leer los contenidos de un mensaje)

    Una estrategia alternativa es utilizar una aplicación de mensajería
cifrada extremo-a-extremo como **Signal **o **WhatsApp**. Para más
detalles ir a la sección de [*Mensajería móvil*](#gj182uwvt4z5).

<a name="ups1ji8w8qx"></a>

##### III. G Suite

Debemos familiarizarnos con la [*Configuración para compartir*](https://support.google.com/drive/answer/2494893?co=GENIE.Platform%3DDesktop&hl=es) de Google Docs. Si estamos compartiendo un documento de Google con toda el equipo de {ORG} o queremos dar acceso a todos, debemos compartir el enlace como “Cualquiera con enlace dentro de {ORG}”

Debemos evitar compartir documentos con la opción “Cualquiera con
enlace”, ya que esto permite el acceso a cualquiera fuera de la
organización lo cual presenta un riesgo de seguridad dado el tipo de
información sensible que almacenamos en nuestro Google drive

<a name="jsgpqgjiss5a"></a>

#### e. Revocando acceso a los activos al salir de la organización

-   En el último día de trabajo, el encargado de Operaciones deberá
    pedir al empleado que configure un mensaje de “Fuera de oficina”
    en su correo electrónico indicando que ya no trabaja más en {ORG}
    e incluyendo información de contacto del encargado. Este correo
    debe enviarse a empleado de contacto y otros miembros del equipo
    que sea relevante. Al final del día de trabajo, el encargado de
    Operaciones cambiará la contraseña de la cuenta de correo
    electrónico del empleado y de otras cuentas a las que se
    tenga acceso.

-   La dirección de correo permanecerá activa por 2-3 meses, después de
    este periodo será removida permanentemente y los documentos de
    Google/Dropbox asociados a esta cuenta serán transferidos a la
    persona apropiada.

-   Las cuentas de Dropbox, Skype (si {ORG} la configuró) y otras
    cuentas serán canceladas el último día de trabajo.

-   Las contraseñas que hayan sido compartidas con el empleado deberán
    ser cambiadas e informadas a otros miembros del equipo a
    quienes competa.

-   Todos los activos propiedad de la organización deberán ser
    devueltos, llaves y tarjetas de acceso deberán ser devueltas.

<a name="qi8barmv5j12"></a>

### 2. Políticas de administración de datos

<a name="d1n4qtbvkrzi"></a>

#### a. Respaldo de datos

Todos los colaboradores deben asegurar que los documentos e información
relacionados con su trabajo deben ser compartidos en \[Google drive/Dropbox\]

El {Director de Operaciones de ORG} realizará un respaldo completo de
todos los documentos en \[Google drive/Dropbox\] una vez al mes.

Los respaldos de documentos y sistemas personales pueden realizarse en
medios externos como discos duros o memorias USB. Debemos asegurar que
el medio se encuentra [*cifrado*](#x4056g72bwvp) antes de copiar los
archivos al mismo.

Estrategias de respaldo para:

- [Mac OSX: Time Machine](https://support.apple.com/es-la/HT201250)

- [iOS](https://support.apple.com/es-la/HT203977)

<a name="tcdwcnkg8nw6"></a>

#### b. Transferencia de datos

Al transferir datos o información confidencial debe realizarse de manera
segura ya sea colocándola en un medio externo y entregándolo
personalmente al receptor deseado o por medio de correo electrónico
cifrado.

Todas las partes involucradas deben estar al tanto de la transferencia y
esperar los datos antes de concederles acceso a la información. Estar a
la espera de archivos adjuntos ayuda a prevenir ataques de *Phishing*
donde se utiliza los adjuntos para transmitir malware.

- **Email**

    Transmitir datos por Internet es posible mediante el uso de correo
electrónico cifrado o encriptando el archivo antes de enviarlo.
Debemos asegurarnos que la persona que recibe el mensaje está al tanto
de que los archivos están en camino. Es importante recordar que los
metadatos como el asunto, y las direcciones de origen y destino no son
cifradas, por lo que debemos evitar incluir información privada en
estos campos.

- **Medios externos (USB, disco duro, etc.)**

    Antes de conectar el dispositivo a nuestra máquina debemos verificar
quien fue la última persona en utilizarlo y aproximadamente que se
encuentra almacenado en el mismo. IMPORTANTE: Nunca conectar un
dispositivo desconocido a nuestra computadora.

    Cualquier medio externo puede ser utilizado para transferir datos,
pero luego de la transferencia, la información debe ser borrada del
medio externo. Los archivos confidenciales deben estar cifrados o todo
el disco. Debemos tratar de mantener los medios externos con nosotros
en todo momento posible.

<a name="kebchkxtl2yd"></a>

#### c. Viajes

Cuando es necesario viajar debemos tomar precauciones adicionales. Esta
sección está enfocada únicamente en el manejo de datos. Para ver la
recomendaciones completas ir a la sección [*Políticas y procedimientos
de viaje*](#diwraoi9bck).

-   Al utilizar redes WiFi públicas o inseguras, e.g. Hoteles,
    restaurantes, aeropuertos, siempre se debe utilizar una VPN
    (*Virtual Private Network*). Esto aplica para todos los
    dispositivos incluyendo los teléfonos móviles.

-   Los dispositivos pueden ser revisados o decomisados, por este motivo
    debemos asegurarnos que los mismos cuentan con cifrado de disco
    completo (incluidos los smartphones), los dispositivos deben
    permanecer apagados al cruzar una frontera. Para más detalles ver
    la sección de* *[*Cifrado de dispositivos*](#g47kclkb06ga)*.*

-   Para transportar datos de manera segura, debemos asegurarnos que
    todos los medios externos se encuentran cifrados, verificar las
    contraseñas en laptops y teléfonos móviles.

-   Si estamos transmitiendo datos por correo electrónico, debemos
    encriptar tanto el mensaje como los archivos.

-   Evitar llevar dispositivos con información sensible o comprometedora
    en nuestros viajes, si esto es inevitable, debemos dejar en casa
    toda aquella información que no será utilizada o que no es
    necesaria para el viaje.

<a name="3s9d98tikn3q"></a>

#### d. Borrados de datos

Cuando la información es borrada, sigue siendo vulnerable en el disco
hasta que sea sobreescrita. Si es importante que un archivo no sea
recuperable debemos seguir las prácticas de borrado seguro de archivos:

- [Mac OSX](https://ssd.eff.org/es/module/c%C3%B3mo-borrar-tu-informaci%C3%B3n-de-forma-segura-mac-os-x)

- [Windows](https://ssd.eff.org/es/module/borrador-seguro-para-windows)

<a name="g47kclkb06ga"></a>

#### e. Cifrado de dispositivos

El Cifrado de dispositivos (o cifrado de disco completo o FDE por sus
siglas en inglés) puede proteger nuestra computadora, teléfono móvil y
tabletas de acceso físico en caso de que sea decomisado, extraviado o
robado. El dispositivo *debe estar apagado *para que activar la
protección.

Debemos habilitar el cifrado de disco en todos los dispositivos que
contengan información sobre {ORG} information y acceso a cuentas (e.g.
email)

Habilitar el cifrado en teléfonos y tablets:

-   [*Android*](http://www.howtogeek.com/141953/how-to-encrypt-your-android-phone-and-why-you-might-want-to/)
-   [*iOS*](https://ssd.eff.org/es/module/c%C3%B3mo-encriptar-su-iphone)
-   [*Windows*](http://www.windowscentral.com/how-enable-device-encryption-windows-10-mobile)

Laptops / computadoras

-   Mac: Cifrado con [*FileVault*](https://support.apple.com/en-us/HT204837)
-   Windows: Cifrado con [*BitLocker*](http://www.howtogeek.com/234826/how-to-enable-full-disk-encryption-on-windows-10/)
-   Linux: Habilitar el cifrado durante la instalación

<a name="x4056g72bwvp"></a>

#### f. Cifrado de medios extraibles

Si tenemos dispositivos USB, discos duros externos con información
sensible, debemos asegurarnos que estos se encuentren encriptados y
almacenados en un lugar seguro

-   Mac: Cifrado con [*FileVault*](http://apple.stackexchange.com/a/181105)

-   Cross-platform: [*VeraCrypt*](https://securityinabox.org/en/guide/veracrypt/os-x)

Una vez que la información ha cumplido su objetivo y no es necesaria
debemos [*borrarla de manera segura*](#3s9d98tikn3q).

Nunca debemos prestar medios externos a amigos o personas fuera de la
organización, y nunca conectar un dispositivo USB o disco duro
desconocidos a nuestra computadora.

<a name="2mjrr3say9pg"></a>

### 3. Políticas y prácticas de comunicación

Al comunicarnos con otros miembros del equipo, colegas y socios, debemos
estar conscientes del contexto ¿Está nuestro colega de viaje? ¿En qué
país se encuentra? ¿Estoy poniendo a mis compañeros en riesgo al enviar
este mensaje? Debemos tener cuidado al elegir cuando y como
comunicarnos.

<a name="v1i5hop72acs"></a>

#### a. Comunicación en línea

-   **Correo electrónico**

    Los campos “De” y “Para” en los correo electrónicos así como el
asunto, son metadatos visibles, por lo que nunca debemos poner
información sensible o privada en ellos. Los mensajes de correo
electrónico en general no son muy seguros, por esto cualquier
información que sea muy sensible o privada deberá ser transmitida por
un medio alternativos como un archivo cifrado o un mensaje verbal.

    Debemos tener en mente que nuestra dirección de correo del trabajo
puede alertar a personas maliciosas sobre la existencia y localización
de nuestros compañeros. Debemos considerar otros canales de contacto
si es necesaria una comunicación más discreta.

-   **Llamadas de voz y video**

    Las llamadas de voz puede ser una forma de comunicación rápida y
eficiente. Usemos [*Google Hangouts*](https://support.google.com/hangouts/answer/3110347) cuando sea posible (permite un [*máximo de 25
participantes*](https://support.google.com/plus/answer/1216376?hl=es)),
y [*https://meet.jit.si/*](https://meet.jit.si/) como respaldo.

    Se debe evitar el uso de Skype y utilizar Google Hangouts como el
medio de comunicación por defecto para llamadas de voz y video. Skype
tiene varios problemas que facilitan que los usuarios sean blanco de
ataques o compromiso de cuentas, entre esos problemas se encuentran:

    -   Es fácil impersonar a otro usuario
    -   Al ser una aplicación independiente en lugar de un servicio que
    corre en el navegador
        -   Al abrir archivos adjuntos por medio de una aplicación de
        escritorio en vez de un servicio web (e.g. Google drive) se
        incrementa el riesgo de contagio de malware

    -   Permite ver la localización geográfica (dirección IP) de los
    usuarios

-   **Mensajería móvil**<span id="anchor-23"></span>**Mensajería móvil**

    **Signal **es una buena aplicación para comunicarse entre los miembros del equipo ya que nos ofrece mensajes cifrados de extremo-a-extremos. Debemos estar seguros de utilizar la última versión:

[*https://whispersystems.org/blog/just-signal/*](https://whispersystems.org/blog/just-signal/)

Signal además ofrece una versión de escritorio que lo convierte en una
alternativa válida para Google Hangouts.

Para más información visitar el sitio de Open Whisper Systems [*Signal
support page*](http://support.whispersystems.org/hc/en-us) o el [*FAQ de Access Now*](https://gitlab.com/AccessNowHelpline/helpline_documentation_resources/blob/master/advice/faq_signal.md).

<a name="l3uv2efruza2"></a>

#### b. Comunicación con personas en riesgo

Es importante estar al tanto del contexto de seguridad de nuestros
colegas cuando nos comunicamos con ellos. Cuando tratamos de proteger
su trabajo siempre debemos (1) *seguir su ejemplo *y (2) ofrecer
*opciones de comunicación*.

Nadie conoce su contexto y las amenazas que enfrentamos mejor que
nosotros mismos. Al mismo tiempo, nuestros compañeros pueden tener
necesidades de seguridad que solo pueden ser alcanzadas con nuestra
colaboración y prácticas de comunicación segura. Informarles sobre los
diferentes medios de comunicación que podemos utilizar -- e.g.
utilizar cuentas de correo ajenas a {ORG}, encriptar el contenido de
los mensajes, proteger la identidad y localización de nuestros
colegas, etc -- puede ayudarles a tomar mejores decisiones.

Debemos tener claros los flujos de comunicación para personas
particulares con las que nos comunicamos.

En caso de ser necesario, contactar a la Línea de Ayuda de Access Now
([*help@accessnow.org*](mailto:help@accessnow.org)) para definir una
estrategia personalizada para cualquier situación que se considere
requiera de prácticas especiales o si nuestro contacto necesita ayuda
implementando una práctica en particular.

<a name="fld08f9e02f3"></a>

#### c. Comportamiento en la web

- **Contenido público**

    Cuando publicamos contenido, ya sea fotografías personales en las
redes sociales o material promocional para la organización, debemos
tener cuidado con la información personal que puede ser revelada.
¿Tenemos autorización de las personas en la foto o el fotógrafo para
utilizar la fotografía? Debemos revisar el contenido de fondo de la
foto ya que puede contener información relevante (contraseña escrita
en papel, información de contactos, etc). Debemos tener cuidado de
nunca revelar información que pueda identificar a unos de nuestros
colaboradores y cuando exista duda sobre si algo califica como privado
o no, debemos optar por no publicarlo.

-   **Enlaces y archivos adjuntos**

    Cuando enviamos mensajes que contienen enlaces a sitios web o archivos
adjuntos, debemos avisar al destinatario por un medio alternativo,
como una llamada telefónica o mensaje de texto, para que esté al
tanto. Esto permite que el receptor del mensaje se asegure de que no
se trata de un ataque de *Phishing.*

    Cuando recibimos un mensaje que contiene enlaces y/o archivos
adjuntos, debemos ser cuidadosos y si no estamos seguros de un
mensaje, su origen o de la persona que nos lo envía, no debemos hacer
clic en los enlaces o abrir los adjuntos. Debemos hacer un esfuerzo
por verificar este tipo de mensajes preguntando a la persona que nos
lo envió por un medio alternativo y confirmando la veracidad del mensaje.

<a name="lk3pokycowon"></a>

#### d. Navegación web

- **VPN**

    Cuando navegamos por Internet utilizando una red insegura o desconocida,
cuando estamos de viaje, en cafés, aeropuertos, hoteles, restaurantes,
etc. debemos utilizar una VPN (Virtual Private Network) en todo momento.

    {ORG} debe asegurarse de proveer una cuenta de VPN para cada empleado y
dar el soporte inicial para configurarla. Las [*VPNs nos ofrecen una
forma segura de conectarse a
Internet*](http://lifehacker.com/5940565/why-you-should-start-using-a-vpn-and-how-to-choose-the-best-one-for-your-needs)
para navegar con menor peligro. Nuestro tráfico hacia sitios web o
servicios en línea parecerá proceder de una localización distinta a
donde nos encontramos y todo los datos que enviemos y recibamos estarán
protegidos de ser interceptados en la red local donde nos encontremos
(hotel, aeropuerto, etc)

    Si se desea, se puede utilizar una VPN todo el tiempo, aun en nuestro
hogar u oficina. La VPN se puede utilizar en todos nuestros
dispositivos, incluyendo teléfonos, tabletas, laptops, etc.

-   **Extensiones del Navegador**

    Considere agregar extensiones a su navegador que ayuden a mejorar la
privacidad como los bloqueadores de publicidad y anti-rastreo, también
debemos preferir las conexiones con HTTPS. Ejemplos de extensiones
incluyen [*Privacy Badger*](https://www.eff.org/privacybadger), [*HTTPS
Everywhere*](https://www.eff.org/https-everywhere), y adblockers como
uBlock Origin
([*Firefox*](https://addons.mozilla.org/addon/ublock-origin/),
[*Chrome*](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm)).

<a name="gqy8gv5w1rzc"></a>

#### e. Preocupaciones de seguridad digital

- **Correos sospechosos o indicios de Phishing**

    Si sospechamos que hemos recibido un correo de Phishing, o que no se> un mensaje legítimo, no debemos dar clic en ningún enlace ni descargar ningun archivo adjunto que este contenga. Debemos alertar a {Director de Operaciones de ORG}.

    Después de alertar a {Director de Operaciones de ORG}, reenviar el
mensaje original (con los encabezados) al equipo de la Línea de Ayuda
de Access Now a la direccion [*help@accessnow.org*](mailto:help@accessnow.org) con la explicación de la situación y los detalles de la preocupación.

    Para obtener los encabezados completos del mensaje seguir las
instrucciones a continuación: [*https://support.google.com/mail/answer/29436?hl=es*](https://support.google.com/mail/answer/29436?hl=es)

    De nuevo, debemos ser extremadamente cuidadosos de no hacer clic en
los enlaces o descargar los adjuntos del mensaje sospechoso, y debemos
recordar alertar a las personas a quienes reenviemos el mensaje sobre
los peligros implicados.

-   **Emergencias de Seguridad Digital**

    Si ocurre una emergencia física, debemos alertar al Director Ejecutivo y revisar los protocolos de seguridad para identificar los canales de contacto apropiados.
    
    Si existe alguna preocupación sobre un potencial problema de seguridad digital, incluyendo abrir un enlace malicioso o adjunto, debemos informar al Director Ejecutivo y al Director de Operaciones lo antes posible.

    Es de vital importancia alertar inmediatamente sobre las amenazas de
seguridad digital que encontremos. Puede ser difícil admitir cuando
comentemos un error, pero si creemos que hemos puesto en riesgo a la
organización o a un miembro del equipo, es mejor informar a los demás
para que el daño pueda ser mitigado. Contarle a alguien sobre nuestro
error lo antes posible es lo mejor que podemos hacer en estas
situaciones.

<a name="cwhasyrukd3s"></a>

### 4. Políticas y procedimientos de viaje

Cuando nos encontramos de viaje, es de especial importancia estar
conscientes de nuestros alrededores y conocer las leyes locales de
nuestro destino. En cualquier momento determinado, especialmente cuando
cruzamos una frontera, nuestros dispositivos pueden ser revisados o
tomados fuera de nuestro alcance.

Revisar la siguiente información:
[*https://ssd.eff.org/es/module/cosas-considerar-al-cruzar-la-frontera-de-los-estados-unidos*](https://ssd.eff.org/es/module/cosas-considerar-al-cruzar-la-frontera-de-los-estados-unidos)

<a name="66ctgfndt3o"></a>

#### a. Dispositivos

[***El Cifrado de disco completo***](#g47kclkb06ga)** es esencial **en
todos los teléfonos móviles, laptops y tabletas. Funciona solo cuando
los dispositivos se encuentran apagados por lo cual debemos asegurarnos
de apagarlos antes de cruzar una frontera o atravesar un punto de
control.

**No llevar dispositivos que no necesitamos. **Debemos llevar con
nosotros exclusivamente aquellos dispositivos que son esenciales para
realizar nuestro trabajo. Debemos evitar viajar con dispositivos que
contengan información particularmente sensible o confidencial.

**Transportar datos sólo en los dispositivos encriptados**.Si utilizamos
un medio externo, como memorias USB, tarjetas SD, discos duros, debemos
cifrar el dispositivo antes de colocar ninguna información en él.

Revisar que los dispositivos estén protegidos con **contraseña, PIN o
códigos** de acceso.

<a name="wf4menzgiaqi"></a>

#### b. Uso de Internet

Cuando naveguemos el Internet lejos de casa, es indispensable utilizar
siempre una VPN al conectarnos. Debemos asegurarnos de instalar la VPN
en todos los teléfonos, laptops y tabletas que llevaremos con nosotros.

Borrar el cache, cookies e historial de todos los navegadores que
utilizamos (incluyendo los teléfonos móviles):

- [*Firefox*](https://support.mozilla.org/es/kb/limpia-la-cache-y-elimina-los-archivos-temporales-)

- [*Chrome*](https://support.google.com/chrome/answer/2392709?hl=enhttps://support.google.com/chrome/answer/95582?hl=es)

- [*Other Browsers*](https://kb.iu.edu/d/ahic)

Clear passwords from all browsers:

- [*Chrome*](https://support.google.com/chrome/answer/95606?hl=es)

- [*Firefox*](https://support.mozilla.org/es/kb/administrador-de-contrasenas-recordar-borrar-cambiar-importar-contrase%C3%B1as-firefox)

<a name="6bqo8dqlb1im"></a>

### 5. Creando y manteniendo una cultura organizacional comprometida

Para mantener la seguridad de la organización, debemos tener una buena
“higiene de seguridad” en mente. Instalar actualizaciones en nuestros
dispositivos de manera rápida y constante, estar conscientes de nuestros
alrededores y la ubicación de nuestros dispositivos (en el mejor de los
casos, estos están siempre con nosotros). Debemos considerar instalar
extensiones a nuestro navegador que ayuden a incrementar su seguridad
como ad-blockers y discutir con nuestros colegas sobre sus prácticas de
seguridad.

Compartir noticias relevantes de fuentes acreditables, Sharing relevant
news articles from reputable media, [*juegos y quizzes
interactivos*](https://orgsec.community/display/OS/Ways+to+build+awareness+around+security+issues)
, y
[*testimonios*](https://orgsec.community/display/OS/Why+should+the+organisation+and+its+staff+care+about+security)
pueden ayudar a involucrar a las personas. Debemos tener canales de
comunicación abiertos con los colaboradores para garantizar que
cualquier duda o incidente se atiende de forma rápida.

Recordemos que todos los miembros de la organización están conectados.
Nuestras prácticas en casa afectan nuestra seguridad en la oficina.
Debemos mantener seguras nuestras cuentas de correo electrónico y redes
sociales personales tanto como las de trabajo. Las prácticas mencionadas
en este documento pueden ser aplicadas a nuestros dispositivos
personales.