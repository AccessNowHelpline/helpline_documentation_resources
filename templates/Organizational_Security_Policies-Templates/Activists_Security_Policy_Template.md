Security Policy for Activists and Grassroots Groups - Template
=================


Baseline organizational practices
-------------------

1. [Access policies](#7we3ntqs92fn)
    - [a.  Your workspace](#llduhfrf8ye0)
        - [I. Working from public spaces](#5cy5di7ujjex)
        - [II. Working from home](#corws3uqqxe7)
        - [III. Meetings](#meetings)
    - [b. Access to physical documents](#nu5wl7mbnh5)
        - [I. Printed documents](#u9fp01cjk71z)
        - [II. Destruction of papers](#ohynrrsowkja)
    - [c. Devices](#cneaewojyb0j)
        - [I. Dedicated devices](#4dhi7dsh04tw)
        - [II. Access to devices](#wu4v2aytkvmo)
    - [d. Access to internet](#jnwl7ij2k1i5)
    - [e. Access to accounts](#v5nwlrejiitd)
        - [I. Two-Factor Authentication](#wtfwcz9ih5gb)
        - [II. Password management](#6r1cvdme4q45)
        - [III. Facebook practices](#ups1ji8w8qx)
        - [IV. Facebook Messenger practices](#by1fkmcflt74)
        - [V. WhatsApp practices](#elfwvyl6semr)
    - [f. Revoking access to assets upon departure](#jsgpqgjiss5a)

2. [Data management policies](#qi8barmv5j12)
    - [a. Transferring data](#tcdwcnkg8nw6)
    - [b. Travelling](#kebchkxtl2yd)
    - [c. Deleting data securely](#3s9d98tikn3q)
    - [d. Device encryption for all devices](#FDE)
    - [e. Protect your devices against malware](#x4056g72bwvp)

3. [Communications policies & practices](#2mjrr3say9pg)
    - [a. Online communication](#v1i5hop72acs)
    - [b. Communicating with at-risk partners and beneficiaries](#l3uv2efruza2)
    - [c. Internal communications](#internalcomms)
    - [d. Web etiquette](#fld08f9e02f3)
    - [e. Web browsing](#lk3pokycowon)
    - [f. Digital security concerns](#gqy8gv5w1rzc)

4. [Travel policies and procedures](#travel)
    - [a. Devices](#66ctgfndt3o)
    - [b. Internet use](#wf4menzgiaqi)

5. [What to do if a device is lost, confiscated, or stolen](#6bqo8dqlb1im)

6. [Server management](#server)

7. [Creating and maintaining an engaged organizational culture](#engagement)


* * *

<a name="7we3ntqs92fn"></a>

### 1. Access policies

<a name="llduhfrf8ye0"></a>

#### a. Your workspace

Whether you work for [ORG] from home or from a public space, consider who can access this space and what they can see or find.

<a name="5cy5di7ujjex"></a>
##### I. Working from public spaces

If you work from a public space, try to hide your keyboard when entering
passwords and consider using a privacy screen to obscure your monitor,
or sit with your back to a wall to avoid someone watching behind your
shoulders.

<a name="corws3uqqxe7"></a>
##### II. Working from home

If you print materials or have storage devices containing data connected
to [ORG] in your home, make sure to leave them in a locked space
when you leave the home and don't leave them in view when you have
visitors. If a stranger is in your home, like a house cleaner or plumber,
please try to avoid allowing access to your workspace. If the stranger
needs workspace access, take precautions before the visit (lock
confidential papers away, remove laptops and turn off computers).

When you have finished your shift or your work for [ORG], switch off and
place out of sight any device dedicated to your [ORG] activities.

<a name="meetings"></a>
#### III. Meetings

During meetings and other events where sensitive activities are
going on, it's ideal to leave your telephone at home. If you need to
take it with you, switch it off and leave it in a different room during
sensitive face-to-face communications.


<a name="nu5wl7mbnh5"></a>

#### b. Access to physical documents

<a name="u9fp01cjk71z"></a>

##### I. Printed documents

Any internal printed documents with [ORG] information should be
treated as confidential, and cared for appropriately. If you print
materials connected to [ORG] with a shared printer, make sure to collect
the printouts immediately and to not leave them unattended. In any case,
try to be aware of how private particular information is before you
print it -- especially if you are creating copies.

<a name="ohynrrsowkja"></a>

##### II. Destruction of papers

If a document is printed, as soon as it is no longer needed please
destroy in a paper shredder as soon as possible.

<a name="cneaewojyb0j"></a>

#### c. Devices

<a name="4dhi7dsh04tw"></a>

##### I. Dedicated devices**

If you can, use different devices than your personal ones to connect to
[ORG] services, communicate with other activists, and respond to
calls.

If you only have one computer, consider using a [Tails
stick](https://tails.boum.org/) for every activity related to [ORG]. If you need to store data, you can create a [persistent encrypted volume in your Tails stick](https://tails.boum.org/doc/first_steps/persistence/index.en.html).

Try to reduce the number of devices where you store information
connected to your activity for [ORG], including the credentials to
your email. Every new device with confidential [ORG] accounts or data is a
new device that will require protection from loss, theft, and hacking.
If possible, a smartphone and computer (laptop, desktop, Tails stick)
should be the only two devices used for [ORG].

[ORG] has provisioned phones for activists to ensure that
confidential information stays on those devices, and is not transferred
to private devices. In countries where this is possible, these phones
contain anonymous SIM cards to make sure that your [ORG] activity is not 
connected to your official identity. In countries where this is not possible,
the SIM cards might be bought under an organization's name, if available.
If none of these options is available to you, it is recommended to use an
alternative phone for your [ORG] activities.

<a name="wu4v2aytkvmo"></a>
##### II. Access to devices

Ensure that you are the only one who can access the device containing
information related to [ORG] and that shared [ORG] mobile phones are only
accessed by trusted [ORG] activists. If there is confidential data on the
same laptop that others who are not involved with [ORG] use, please remove
it.

Include a PIN or passphrase to access any device you have with [ORG]
information on it, including smartphones.

When device encryption is enabled, a screen lock passphrase (or PIN) is
also created by default. Please see the [device encryption
section](#device-encryption) for further details.

<a name="jnwl7ij2k1i5"></a>
#### d. Access to internet

Be aware of who has access to your internet connection. If you are in a
shared or public space (cafes, airports, hotels, libraries, social
centres, squats...) where the Wi-Fi network is open or the Wi-Fi
password is posted or freely handed out to guests, the network should be
considered untrustworthy, and appropriate action to protect oneself
should be taken (see [Web Browsing](#web-browsing)), including using a
VPN or Tor for all your activities connected to [ORG].

Working in a “trusted” environment requires complete knowledge of who is
on the network with you, with minimal protections including a guest
network being available, secured with WPA-2 password, and a changing of
all third-party provided default settings (like the default router
password and the admin panel’s default password, etc.).

<a name="v5nwlrejiitd"></a>
#### e. Access to accounts

<a name="wtfwcz9ih5gb"></a>
##### I. Two-Factor Authentication

Activists should enable 2-factor authentication whenever possible for
the accounts connected to their [ORG] activities. Whenever possible,
SMS-based 2-factor authentication should be avoided and an authenticator
app like [FreeOTP](https://freeotp.github.io/) or a security key should
be preferred.

Services that offer 2-factor authentication include for example:

- Facebook
    - https://www.facebook.com/help/loginapprovals
- Gmail
    - https://www.google.com/landing/2step/
- Outlook, msn.com, hotmail.com
    - https://www.eff.org/de/deeplinks/2016/12/how-enable-two-factor-authentication-outlookcom-and-microsoft
    - https://www.msoutlook.info/question/773
- Protonmail
    - https://protonmail.com/support/knowledge-base/two-factor-authentication/
- xs4all
    - (instructions only in Dutch) https://www.xs4all.nl/service/diensten/email/installeren/geavanceerd/inloggen-met-otp-calculator.htm
- Tutanota
    - https://tutanota.uservoice.com/knowledgebase/articles/1201942-how-does-two-factor-authentication-2fa-work-in-t
- Mailbox.org
    - https://userforum-en.mailbox.org/knowledge-base/article/is-there-a-two-factor-authentication
- icloud.com
    - https://support.apple.com/en-us/HT204915
- gmx.de
    - (instructions for securing account access without 2FA) https://www.henning-uhle.eu/informatik/was-wurde-aus-der-zwei-faktor-authentifizierung-bei-gmx\

<a name="6r1cvdme4q45"></a>
##### II. Password management

In order to keep shared passwords secure, [ORG] activists share
passwords through encrypted channels and store them in their private
password manager (KeePassXC). [ORG] may consider storing shared
passwords in a secure team password manager that may be self-hosted in
their servers, like [Bitwarden](https://help.bitwarden.com/article/install-on-premise/) or [Passbolt](https://help.passbolt.com/hosting/install).

Each service should have a unique password and should follow the
responsible password generation guidelines noted below.

- **Personal Passwords**

    Responsible password creation and use can be better understood reading
the following manuals:

    -   https://ssd.eff.org/en/module/creating-strong-passwords
    -   https://securityinabox.org/en/guide/passwords

    The primary takeaways are:

    -   **Long:** over 15 characters
    -   **Strong:** upper and lower case letters, numbers, and symbols
        should be included
    -   **Always unique:** never reuse a password

    Intimidated by these requirements? Consider using [this
    comic](https://xkcd.com/936/) for inspiration.

- **Password Managers**

    In order to manage personal passwords and ensure that all your passwords
are long, strong, and unique, you should use a password manager.

    The recommended personal password manager is [KeePassXC](https://keepassxc.org).

    - To learn how to use it, you can read [this guide](https://ssd.eff.org/en/module/how-use-keepassxc)

- **Sharing Passwords**

    If you must share passwords with another activist, please use a password
    sharing strategy that reduces the risk that the password will be
    recorded by a third party or compromised.
    
    In particular, sharing it on a piece of paper works if the password is
    then inputted into the receiver’s personal KeePassXC and the paper
    shredded.
    
    Using third-party services like Keyvault is not recommended because you
    are sharing your password with a third-party and their network. The
    content is not end-to-end encrypted (end-to-end encryption ensures that
    only the sender and recipient know the content of the message).
    
    Using an end-to-end messaging application such as Signal or WhatsApp
    could work. Please see the Mobile Messaging section for further
    details.
    
    Installing a free and open source password manager like
    [Bitwarden](https://help.bitwarden.com/article/install-on-premise/) or
    [Passbolt](https://help.passbolt.com/hosting/install) in [ORG]'s own server
    would also be a good solution.

<a name="ups1ji8w8qx"></a>
##### III. Facebook practices

Public communications by [ORG] on [ORG]'s page should be
posted through [ORG]'s official account. Any other account used to
manage [ORG]'s page should be different and separate from
activists' private accounts and possibly use a pseudonym for their user
name.

Accounts used to manage [ORG]'s page should not be followed by the
owner's private account and should not share the same contacts. These
accounts should be private and tagging should be disabled.


<a name="by1fkmcflt74"></a>
##### IV. Facebook Messenger practices

Facebook Messenger can also be used for end-to-end encrypted
communications, but this feature is not the default. Secret messages can
be used for sending messages, pictures, stickers, videos, and voice
messages. It cannot be used for group messages and voice or video
calls.

- To start a so called Secret Conversation, you can follow there
    instructions: https://www.facebook.com/help/messenger-app/811527538946901
- More information on Secret Conversations can be found here: https://www.facebook.com/help/messenger-app/811527538946901

<a name="elfwvyl6semr"></a>
##### V. WhatsApp practices

WhatsApp offers end-to-end encryption based on the Signal protocol, but
it stores metadata.

The app can be configured to improve its security and privacy settings.
To reduce risk and increase privacy when using WhatsApp, you should
apply the following settings:

-   Settings -&gt; Account -&gt; Privacy
    -   Last Seen: My Contacts
    -   Profile Photo: My Contacts
    -   Status: My Contacts
    -   Read Receipts: OFF
-   Settings -&gt; Account -&gt; Security
    -   Show Security Notifications: ON
-   Settings -&gt; Chats
    -   Save Incoming Media: OFF
    -   Chat Backup -&gt; Auto Backup: OFF
-   Settings -&gt; Notifications
    -   Show Preview: OFF (unfortunately, this still displays the
        sender’s name)

These settings are a reasonable middle ground for reducing the amount of
data that the app creates locally, and minimizing what is exposed as
clear text on iCloud/Google Drive.


<a name="jsgpqgjiss5a"></a>
#### f. Revoking access to assets upon departure

When an activist decides to stop their activities with [ORG], they should
inform their local group so that proper security measures can be taken.

-   Any shared password will be changed
-   Subscriptions to mailing lists will be revoked
-   Accounts on online platforms will be revoked
-   Access to [ORG]'s Facebook page will be revoked
-   Phones will be returned

<a name="qi8barmv5j12"></a>
### 2. Data management policies

<a name="tcdwcnkg8nw6"></a>
#### a. Transferring data

Transfer confidential information securely either by placing on an
external media device and directly handing off to the intended
recipient, or by sending the information through an encrypted email or an end-to-end instant messaging app like Signal or XMPP with OMEMO or OTR.

Try to ensure all parties involved know to expect the incoming data
before they access it. Knowing to expect an attachment helps to prevent
phishing attempts where an attachment is the source of malware.

-   **Email**

    Transferring data over the internet is possible through encrypted
    email, or by encrypting a file and sending it. Ensure the person who
    is receiving the attachment knows the files are coming. Don’t forget
    that subject lines of emails, as well as the To and From, are not
    encrypted, so never put anything private there.

-   **External Media (USB, hard drives, etc.)**

    Before plugging in, know who had the storage media last and
    approximately what is on it. (Remember: never plug into your computer
    any external media whose origins you don’t know!)
    
    Any external media can be used to transfer data, but after the transfer,
    the data should be securely deleted from the external media. Encrypt
    confidential files, or the entire drive and, while in transit, keep the
    external media on your person as much as possible.

<a name="kebchkxtl2yd"></a>
#### b. Travelling

When travelling, it is essential to take extra precautions. This section
is only about data management. For full travel recommendations, see
["Travel Policies and Procedures"](#travel) below.

If you must use public or unsecured Wi-Fi, always use a VPN (Virtual
Private Network). This is true for all devices, including smartphones.

Recognize that devices may be searched, so ensure full-disk encryption
is enabled on everything (including smartphones), and turn devices off
when moving through borders. Note that full-disk encryption only
protects your computer or phone while it is entirely off. Please see [below for
instructions on how to encrypt](#FDE).

To transport data securely, ensure all external media are encrypted.
Double-check passcodes are enabled on your laptops and phones.

If you are transferring data through email, encrypt the message and the
file.

Try not to bring devices that have private or compromising data on your
travels. If you must, leave any information you will not be using at
home.


<a name="3s9d98tikn3q"></a>
#### c. Deleting data securely

When information is deleted, it is still vulnerable to recovery if not
specifically overwritten. If it is important the files are not
recoverable, please be sure to use secure deletion practices.

- [Mac OSX](https://ssd.eff.org/en/module/how-delete-your-data-securely-mac-os-x)

- [Windows](https://ssd.eff.org/en/module/how-delete-your-data-securely-windows)

- [Linux](https://wiki.ubuntuusers.de/wipe/)

<a name="FDE"></a>
#### d. Device encryption for all devices

Device encryption (also called full-disk encryption or FDE) will protect
your computer, mobile phone, and tablets against physical access  if your device is lost or stolen by encrypting your hard drive.

Before you proceed with full-disk encryption (FDE), it is important to
know that:

-   the encrypted data is only protected when the computer is off. If
    someone skilled gets access to the computer while it is on, in sleep
    mode, or hibernated, they can use several techniques to extract
    data.
-   to enable FDE, you will need the admin password for your device;
-   you should back up all your data before you proceed, because in case
    of failure you might lose the data in the device.
-   keep in mind that malware could defeat the encryption by waiting to
    copy files off of a device until after booting, when you enter the
    decryption password.
-   encryption is only as good as the encryption password. If a device
    is stolen or confiscated, a simple password can be cracked quickly.
-   while encryption can prevent casual access, truly confidential data
    should be kept hidden to prevent any physical access, or cordoned
    away on a much more secure machine. A machine used for everyday
    activities should not be trusted implicitly, even if its entire hard
    drive is encrypted.

What follows is a list of full-disk encryption tools, listed by device
type and operating system:

-   **Windows** - **BitLocker**
    An included option for Windows Ultimate and Enterprise editions of
    Windows Vista and Windows 7, the Pro and Enterprise editions of
    Windows 8 and Windows 8.1, and Windows Server 2008, Windows 10, and
    later.
    -   General guide:
        https://securityinabox.org/en/guide/basic-security/windows/\#windows-full-disk-encryption-with-bitlocker
    -   Instructions for Windows 7:
        https://technet.microsoft.com/en-us/library/ee424299(v=ws.10).aspx
    -   Instructions for Windows 8:
        https://support.microsoft.com/en-us/help/2855131/how-to-enable-bitlocker-device-encryption-on-windows-8-rt
    -   Instructions for Windows 10:
        http://windows.microsoft.com/en-us/windows-10/turn-on-device-encryption

    - BitLocker is not available in all Windows versions. If your Windows
    version does not include Bitlocker, you can consider creating an
    encrypted folder for sensitive files with [VeraCrypt](https://www.veracrypt.fr/en/Home.html).
        - A guide for VeraCrypt: https://securityinabox.org/en/guide/veracrypt/windows


-   **Mac - FileVault 2**

    FileVault 2 is included in all Mac computers running Mac OS X Lion
    and later versions of Mac OS.

    -   You can find instructions for activating FileVault 2 here:
        https://support.apple.com/en-us/HT204837
    -   For older versions - Mac OS X Leopard and Mac OS X Snow
        Leopard - FileVault can be activated:
        https://hilfe.uni-paderborn.de/FileVault\_unter\_Mac\_OS\_X\_Snow\_Leopard.


-   **Linux**
   
    Full-disk encryption is usually an option that can be enabled during
    the installation of the system. If some of [ORG]'s activists run Linux
    machines where FDE has not been enabled during the installation,
    they should consider reinstalling the system and enabling full-disk
    encryption during the installation. Before reinstalling the system,
    data should be backed up. Please reach out to Access Now Helpline
    for any help in this process.


-   **Android**

    In Android 6.0 Marshmallow and later versions, full-disk encryption
    is enabled by default. To double-check that it is enabled, follow
    the steps listed below for older Android versions.
    
    In Android 4.0 Ice Cream to Android 5.0 Lollipop device encryption
    is an option. To enable it, go to: Settings -&gt; Personal -&gt;
    Security -&gt; Encryption. Before enabling device encryption, however, a screen lock password needs to be set up.


-   **iOS - Passcode**

    iPhones with iOS 8 and higher are encrypted by default, but it's a
    good idea to double-check that FDE is enabled.
    
    Encryption is an option available on devices running iOS 4 to iOS 7,
    but it is not enabled by default. To enable it, follow these steps:
    
    1.  Tap Settings &gt; Passcode (or Touch ID and Passcode) &gt; Turn
        Passcode On.
    2.  Follow the prompts to create a passcode.
    3.  After the passcode is set, scroll down to the bottom of the
        screen and look for the phrase "Data protection is enabled".
    4.  From the same window you should set the “Require passcode”
        option to “Immediately”, so that your device isn't unlocked when
        you are not using it.

If someone is using the iTunes option to back up the device on their
computer, it will be necessary to choose the “Encrypt backup” option on
the *Summary* tab of the device in iTunes. All data will be encrypted
by iTunes before it is saved on the computer.

If someone backs up to iCloud, according to Apple, the data is
encrypted. Apple holds the encryption keys. Use a long passphrase to
protect the data, and keep that passphrase safe.

Finally, if someone has critical data on the device and is concerned
about loss or theft, setting the device to wipe all data after 10 failed
passphrase attempts is an option, as is remote erasure of the data. You
will need to activate the auto backup option first.

- **Encryption for external media**

    If USBs, hard drives, or other external media contain sensitive or
confidential information, ensure they are encrypted and kept in safe
locations.

-   Mac: encrypt with FileVault - https://apple.stackexchange.com/questions/179819/how-do-i-encrypt-an-external-drive-with-filevault-2-in-osx-yosemite/181105#181105
-   Cross-platform: VeraCrypt - https://securityinabox.org/en/guide/veracrypt/os-x

    After the information is no longer needed, make sure to [delete it
securely](#secure-deletion).

<a name="x4056g72bwvp"></a>
#### e. Protect your devices against malware

All the above measures can become meaningless if an activist's device is
infected by malware, especially if the attack is
[targeted](https://securityinabox.org/en/guide/malware/#targeted-malware).
To prevent infections, you should be aware of the risk:

-   if you receive an attachment by email or messaging app, don't
    download it unless it is sent by a trusted person and you were
    expecting it.
-   never open links that come from suspicious sources.
-   don't use illegal copies of proprietary software and don't download
    pirated media to the computer or phone you use for [ORG].
-   if you use Mac, iOS, Android, or Linux, always install software from
    the app store or the official repositories.
-   install an antimalware:
    -   on Windows you can use the the built-in [Windows
        Defender](https://support.microsoft.com/en-us/help/17464/windows-defender-help-protect-computer)
        -   [https://support.microsoft.com/en-us/help/17464/windows-defender-help-protect-computer](https://support.microsoft.com/en-us/help/17464/windows-defender-help-protect-computer)
    - on other systems you can use [MalwareBytes](https://www.malwarebytes.com)
-   avoid using untrusted USB devices: never loan external media to
    friends or people outside of [ORG], and never plug a stranger’s USB or
    hard drive into your machine.

<a name="2mjrr3say9pg"></a>
### 3. Communications policies & practices

When communicating with other [ORG] members and partners, be aware of
their context. Is this person traveling? What country are they in? Will it endanger them to receive messages from your email? Be sure to take note when evaluating how best to communicate.

<a name="v1i5hop72acs"></a>
#### a. Online communication

-   **Email**

    The To and From addresses in emails, as well as the Subject of the
    email, may be visible to third-party parties beyond the email
    providers and your intended recipients, so avoid putting sensitive
    information in those fields.

    Be mindful that your work email address may alert malicious actors to your recipient's existence and whereabouts. Consider other channels of contact if a more subtle approach is important.

-   **Video and voice chat**

    Voice communication can be a fast and efficient form of communication with other [ORG] members and with external partners. Options such as Google Hangouts (which allows [*maximum 25 participants*](https://support.google.com/plus/answer/1216376?hl=en)) and [*Jitsi Meet*](https://meet.jit.si/) are often satisfactory choices.

    Try to avoid using Skype as the default for video and chat
    communications. Skype has had a number of poor practices that make it easier for users to be targeted or compromised through it, including:

    - easy impersonation of contacts
    - being a separate program rather than a platform that runs in a browser
        - When you open attachments and files on the desktop rather than in an online service (like Google Drive for example) you are putting yourself at greater risk of compromise
    - enabling contacts to find your geographic location (IP address)

-   **Mobile messaging**

    For mobile messaging, consider using **Signal**, an encrypted and
    easy-to-use mobile messaging application. Signal also has a desktop
    instant messaging option, making it as convenient to use as Hangouts.

    Try [*Signal support page*](https://support.signal.org/hc/en-us) for questions.

<a name="l3uv2efruza2"></a>
#### b. Communicating with at-risk partners and beneficiaries

It is important to be aware of our partners’/beneficiaries' individual security
contexts when we communicate with them. In trying to protect them and
their work, we should always (1) *follow their lead* and (2) *provide
communications options*.

No one will know their context and the threats they face better than
themselves. At the same time, they may have security needs that can
only be met by us having more secure communications practices. Letting
them know different ways that you can communicate with them -- using
non-[ORG] branded accounts, encrypting the content of communications,
protecting the location or identity of the partner, etc. - can help
them make a more informed decision.

We will be drafting some secure communications workflows for particular partners who we work with.

Please reach out to Access Now’s Helpline team ([*help@accessnow.org*](mailto:help@accessnow.org)) to craft a tailored strategy for any situation you think may require heightened practices, or if your partner may need support in implementing a particular practice.

<a name="internalcomms"></a>
#### c. Internal communications

When communicating with other activists, be aware of their context. Is
this person travelling? What country are they in? Will it endanger them
to receive unencrypted messages from your email? Be sure to take note
when evaluating how best to communicate.

- **Email**

    It is a good practice to create an email account dedicated to [ORG]
    activities. All activists are encouraged to create a new account on a
    privacy-friendly provider, like the ones listed in [this page](https://riseup.net/radical-servers).
    
    Download your email to a secure device with full-disk encryption, don't
    leave it on servers unless you know for sure that your mailbox is individually encrypted and cannot be accessed even by the sys admins of the email provider (like on Riseup, Autistici/Inventati, and Immerda, for example).
    
    To download your email, use a mail client like Thunderbird and
    set it to download with [POP3](https://support.mozilla.org/en-US/kb/faq-changing-imap-pop).
    
    Sensitive communications should always be encrypted, so if you're using
    email to exchange sensitive communications, these emails should be
    encrypted with PGP. To enable PGP in your computer, you will need to
    install Thunderbird and an addon called Enigmail and to create a PGP key
    pair. To learn how to do this, you can follow these guides:
    
    -   Windows: https://guides.accessnow.org/PGP_Encrypted_Email_Windows.html
    -   Mac: https://guides.accessnow.org/PGP_Encrypted_Email_Mac.html
    -   Linux: https://guides.accessnow.org/PGP_Encrypted_Email_Linux.html

- **Video and voice chat**

    Voice communication can be a fast and efficient form of communication
    with the organization and with external partners. Options such as Signal, Wire
    and Jitsi Meet are often satisfactory choices.
    
    -   Wire can be registered without a telephone number and used for video
        calls through this [desktop app](https://app.wire.com/).
    -   Jitsi Meet is hosted by several entities. One [trusted
        instance](https://meet.mayfirst.org) is hosted by the grassroots
        tech collective Mayfirst/People Link. [ORG] might consider [hosting it
        in its server](https://jitsi.org/downloads/).
    
    Try to avoid using Skype for video and chat communications. Skype has
    had a number of poor practices that make it easier for users to be
    targeted or compromised through it.

- **Mobile messaging**

    For mobile messaging, consider using Signal, an encrypted and
    easy-to-use mobile messaging application. Signal also has a desktop
    instant messaging option, making it as convenient to use as Hangouts or
    WhatsApp.
    
    If you prefer not to give your telephone number to your contacts,
    consider using Wire instead.


<a name="fld08f9e02f3"></a>
#### d. Web etiquette

- **Public posts**

    When posting publicly, whether personal photos on social media accounts
or promotional material for the organization, please be aware of the
personal information that may be revealed. Do you have the permission of
the people in the photo, or of the photographer, to use their photo?
Check the backgrounds of pictures for revealing information (password on
a post-it? List of partners' emails?), be sure never to reveal
personally identifiable information when discussing [ORG] activities, and
if you’re unsure if something qualifies as private, choose not to post
it.

- **Links and attachments**

    When sending links and attachments, try to alert the receiver through a second channel, like a phone call or text message. This lets the receiver be reasonably sure it is not a phishing attack.

    When receiving links and attachments, try to remain cautious and if you
are unsure of the message, or of the sender, do not click on the link.
Do your best to verify unsolicited links and attachments by asking the
sender through a separate channel if they did indeed send the message.

<a name="lk3pokycowon"></a>
#### e. Web browsing

- **VPN use**

    When browsing the web on any untrusted or open Wi-Fi network, including
    while travelling, in cafes, airports, or hotels, please use a VPN
    (virtual private network) at all times.
    
    VPNs provide a secure connection to the internet for you to browse
    safely. Your network traffic to websites and online services will look
    like it is coming from a different location than where you are, and all
    data that is sent and received will be protected from snooping on the
    local network you are connected to (in the cafe, airport, etc.).
    
    If desired, you can use a VPN at all times, even on your home or office
    network. VPNs may be used on all of your devices, including phones,
    laptops, tablets, and more.
    
    If you need advice on which VPN to use, please reach out to Access Now
    Helpline.
    
-   **Browser extensions**

    Consider adding privacy enhancing browser extensions to block ads and
trackers which can inadvertently deliver malware, and prefer website
connections to HTTPS. Examples include Privacy Badger, HTTPS Everywhere,
and adblockers such as uBlock Origin (Firefox, Chrome).

<a name="gqy8gv5w1rzc"></a>
#### f. Digital security concerns

- **Suspicious email or suspected phishing**

    If you suspect an email is a phishing attack, or it makes you suspicious
    that it is not genuine in any way, don't click on any link or download
    any attachment and please alert [ORG]'s tech team.
    
    After alerting the tech team, please forward the full original email to
    Access Now’s Helpline Team at help@accessnow.org explaining why you
    are concerned.
    
    To forward the full original email, please follow [these instructions](https://circl.lu/pub/tr-07/).
    
    Once again, be extremely careful not to click on the links nor download
    attachments from the suspicious message, and don’t forget to alert
    anyone you are sending the message to about the danger.

-   **Digital security emergencies**

    If a physical emergency happens, please alert the tech team.
    
    If there is any potential digital security concerns, including clicking
    a potential phishing or infected link or attachment, you can get in
    touch with Access Now Helpline for further support.
    
    It is very important to quickly alert someone to any digital security
    threat you perceive. It can be hard to admit when one has made a
    mistake, but if you believe you did something to compromise the
    organization or a partner or member, it is better to let someone know so
    the damage can be mitigated. Telling someone about the mistake promptly
    is the best thing to do.
    
<a name="travel"></a>
### 4. Travel policies and procedures

When travelling, it is especially important to be aware of one’s
surroundings, and the local laws governing the destination. At any
point, especially when crossing borders, your devices may be searched or
taken out of view.

More details on travel security can be found in [this
guide](https://guides.accessnow.org/safer_travel_guide.html).

<a name="66ctgfndt3o"></a>
#### a. Devices

Full-disk encryption becomes essential on all phones, laptops, and
tablets. It works when you turn off your device before crossing a
border, or entering any travel security checkpoint.

Leave devices that you don't need at home. Try to only bring devices
that you are sure are essential to your work. Avoid any devices that
hold particularly sensitive or confidential information.

Transport data only on encrypted media. If you are using external media,
like USBs, SD cards, and hard drives, encrypt the device before storing
anything on it.

Double-check all of your devices have passwords, PINS, and codes for
access.

<a name="wf4menzgiaqi"></a>
#### b. Internet use

When browsing the internet away from home, always use a VPN to connect.
Be sure to install the VPN on all phones, laptops, and tablets you’ll be
bringing with you.

Clear your cache, cookies, and browsing history across all browsers
(mobile phone included):

-   Firefox
-   Chrome
-   Other Browsers

Clear passwords from all browsers:

-   Chrome
-   Firefox

<a name="6bqo8dqlb1im"></a>
### 5. What to do if a device is lost, confiscated, or stolen

If a device has been lost, confiscated or stolen, there are some
immediate measures that can be taken.

First of all, ask yourself if the device had full-disk encryption
activated. If so, and if the password is strong, it will be probably very
difficult to access data stored in the device. For more safety, it's
always a good idea to follow these steps:

-   If you have still access to the linked accounts, change the
    password.

-   If SMS-based 2-factor authentication is enabled and the lost device
    is the smartphone receiving the SMSs or with the code generator app,
    check if another 2FA method has been set up (e.g. backup codes) that
    can be used to log into the account and change the password.

-   Remote erasure of data stored in the device will only be possible if
    it was enabled in advance. If this is the case, you can follow these
    instructions:

    -    Android: https://www.google.com/android/devicemanager

    -   iCloud: https://support.apple.com/en-us/HT201472

    -   On Windows, Mac, and Linux, you can install Prey to wipe data
        remotely: http://help.preyproject.com/article/195-how-does-prey-work

If you need further support to delete accounts or data, please reach out
to Access Now Helpline.

<a name="server"></a>
### 6. Server management

Servers managed by [ORG] should be updated regularly and set up in the most
secure way possible.

The tech team will apply basic security measures such as the ones listed
here: https://www.debian.org/doc/manuals/securing-debian-howto/

Backups of important documents and systems can be completed on external
media, like a hard drive or USB. **Ensure you have encrypted the drive
before placing any backups on it**. Please back up routinely, at least
once a month. More information on backup strategies can be found in
[this guide](https://debian-handbook.info/browse/en-US/stable/sect.backup.html).

<a name="engagement"></a>
### 7. Creating and maintaining an engaged organizational culture

To continue to keep the organization safe and secure, please keep good
"security hygiene" in mind. Update all devices promptly and routinely,
and remain aware of your surroundings and of where your devices are
(hopefully on your person!). Consider installing security enhancing
browser extensions, like ad-blockers, and chat with your group about
their security practices. Speak with them to find out what their
concerns are, and create plans to deal with the immediate threats and
risks they face.

Sharing relevant news articles from reputable media, interactive games
and quizzes, and community stories all help engage different staff. Have
open communications channels with staff to ensure that any questions or
issues are raised early.

Remember how everyone in the organization is connected. Your practices
at home affect your security at work. Keep your personal email and
social media accounts as secure as your work-related ones. You can apply
all of the practices for your work devices discussed here to your
personal devices.


