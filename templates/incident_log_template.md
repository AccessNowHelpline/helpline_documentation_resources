# INCIDENT LOG TEMPLATE 2023

An incident log is a tool designed to help you record and organize
information that can be useful for tracing the trends, impacts, and
origins of events that could compromise your safety.

You can fill this log by yourself, ask someone you trust to fill it with
you or even manage a collective incident log for your collective,
organization, or any advocacy group you are part of that might be facing
some risk due to the visibility or impact of your work.

Use this tool to keep track of any anomalies or circumstances that
undermine your physical, digital and psychological well-being or that
compromise the safety of the environments and tools you use to develop
your work, communicate and organize with others, or ensure your
sustainability.

The information you record could be a game changer in enhancing any
responses to possible threats that could become more identifiable by
keeping track of the incidents you face during a prolonged period of
time. Having more details to strengthen the knowledge you have about
your threat or risk models makes it easier for you -and any security
experts you reach out to- to design security measures tailored to your
specific needs and context.

| DATE | INCIDENT DESCRIPTION | WHERE DID IT TOOK PLACE? | WHO WAS/WERE AFFECTED BY IT? | WHICH ARE ITS CONSEQUENCES OR IMPACTS? | WHO IS RESPONSIBLE OF OR BENEFITS FROM IT? | IS THERE ANY RELATED ACTIVITY? | WAS IT INTERNAL OR EXTERNAL? | WAS IT INTENTIONAL OR ACCIDENTAL?
| --- | --- | --- | --- | --- | --- | --- | --- | --- |
| | Provide any data you feel comfortable with, as well as any information that might have draw your attention regarding this incident or the context surrounding it. | If it was a physical incident, write as much information regarding the time and location where it tool place. If it was a digital incident, register the website and consider making a backup on digital archives as the Wayback Machine to keep an online record. | Was it something that affected a specific person? Was it something that affected an specific group? Was it something that affected a whole community, country or region? | Which are the outcomes of this incident? How does it affect your physical, economic, digital or emotional well-being? How does it affect your work and the work of your community/group? Does it compromise your safety or the safety of the people you are involved with? | If the incident was physical, try to record any information that allows you and others to identify the person or persons responsible from this incident. If the incident was digital it is OK if you only have access to an account, handle or URL to describe this person or persons. It could even be the case that the person or persons behind this incident can be identified with both: their legal names and digital profiles. | During what process did this incident take place? Were you on your way to meet with your peers? Were you working on a publication that might be connected to any sensitive case? Were you running everyday errands? Were you or your community recovering from any public demonstration? Write down any activities that you think might be associated with the incident and its impacts. | Does the person or persons responsible of this incident have any direct relationship with you, your communities or your work? If so, write "internal" and provide details of the relation you have with this person or persons. If not, write "external" and provide any details regarding how that person or persons might know you or the people affected by this incident. | Was it something that originated from a mishap, mistake, technical failure, structural flaw or carelessness? Or was it something intended to specifically undermine any capacity, directed at sabotaging you, your communities or your work? Write as much information you have to support this and share it with others to verify your assumptions or gather more evidence to support it with facts. |